version       = "0.1.0"
author        = "Rasmus Moorats"
description   = "Nim CHIP8 emulator"
license       = "GPL-3.0-or-later"
srcDir        = "src"
namedBin      = {"main": "nim_chip8"}.toTable

requires "nim >= 1.9.1"
requires "sdl2 >= 2.0.4"

task release, "build binary for release":
  exec "nimble build -d:release --passC:-flto --passL:-flto --passL:-s --opt:speed --mm:arc --passC:-ffast-math"

task clean, "clean workspace":
  exec "rm nim_chip8"
