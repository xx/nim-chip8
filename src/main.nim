import os
import def, cpu, gfx, rom

when isMainModule:
  let params = commandLineParams()
  if params.len == 0:
    quit(-1)

  var memory: array[MEM_SIZE, uint8]
  if not memory.load(params[0]):
    quit(-1)

  var c = CPU(mem: memory)
  c.init
  c.spawn
