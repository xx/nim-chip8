# CHIP8 emulator in Nim

(Almost) pure Nim implementation of a CHIP-8 emulator. Graphics and input is handled by SDL2.

### Building

Run `nimble build` to build a debug release, or `nimble release` to build a binary optimized for speed.

The binary is built as `nim_chip8`.

### Using

```
./nim_chip8 <path_to_rom>
```

Keyboard keys 1-9 (top row and numpad), A-F are mapped to their corresponding keys in the emulator.

### Test suite results

These are the results of various [CHIP-8 test suite](https://github.com/Timendus/chip8-test-suite) tests.

[Corax89's opcode test](https://github.com/Timendus/chip8-test-suite#corax89s-opcode-test):

![Corax89's opcode test](https://nns.ee/blog/img/chip8-test1.png)

[Flags test](https://github.com/Timendus/chip8-test-suite#flags-test):

![Flags test](https://nns.ee/blog/img/chip8-test2.png)

[Quirks test](https://github.com/Timendus/chip8-test-suite#quirks-test) (XO-CHIP):

![Quirks test](https://nns.ee/blog/img/chip8-test3.png)
